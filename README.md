# Hello Express

This project contains REST API with following features:

- Welcome endpoint
- Multiply endpoint

## Getting started

First install the dependencies:

```sh
npm i
```

Start the REST API service:

```sh
node app.js
```

Or in the latest NodeJS version (>=20):

```sh
node --watch app.js
```
